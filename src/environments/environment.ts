export const environment = {
  webview: {
    url: 'https://demo.biapi.pro/2.0/auth/webview/',
    lang: 'en',
    clientId: 56909451,
    redirectUri: 'http://localhost:4200/?state=&types=bank'
  },
  api: {
    endpoint: 'https://demo.biapi.pro/2.0/'
  },
  production: false
};
