export interface IAccount {
    formattedBalance: string;
    originalName: string;
    accountNumber: string;
}
