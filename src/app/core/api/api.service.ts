import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export abstract class ApiService {
  apiEndpoint: string = environment.api.endpoint;
  protected constructor(
      private http: HttpClient
  ) { }

  protected get(route: string) {
    return this.http
        .get(this.apiEndpoint + route);
  }
}
