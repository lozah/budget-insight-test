import { Injectable } from '@angular/core';
import {ApiService} from '../api.service';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {AccountsSerializer} from '../../serializers/accounts.serializer';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends ApiService {
  constructor(http: HttpClient) {
    super(http);
  }

  getUserAccounts() {
    const route = 'users/me/accounts';
    return this.get(route)
        .pipe(
            map(data => new AccountsSerializer().fromJson(data['accounts']))
        )
  }
}
