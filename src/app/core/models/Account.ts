import {IAccount} from '../interfaces/IAccount';

export class Account implements IAccount {
    private _formattedBalance: string;
    private _accountNumber: string;
    private _originalName: string;

    constructor(formattedBalance: string, accountNumber: string, originalName: string) {
        this._formattedBalance = formattedBalance;
        this._accountNumber = accountNumber;
        this._originalName = originalName;
    }

    get formattedBalance(): string {
        return this._formattedBalance;
    }

    set formattedBalance(value: string) {
        this._formattedBalance = value;
    }

    get accountNumber(): string {
        return this._accountNumber;
    }

    set accountNumber(value: string) {
        this._accountNumber = value;
    }

    get originalName(): string {
        return this._originalName;
    }

    set originalName(value: string) {
        this._originalName = value;
    }
}
