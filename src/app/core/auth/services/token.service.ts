import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(
      private cookieService: CookieService
  ) { }

  setToken(token: string) {
    const decodedToken = decodeURIComponent(token);
    this.cookieService.set('token', decodedToken);
  }

  getToken(): string {
    return this.cookieService.get('token');
  }

  deleteToken() {
    return this.cookieService.delete('token');
  }
}
