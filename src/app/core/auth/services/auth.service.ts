import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      private tokenService: TokenService,
      private router: Router
  ) { }

  isLoggedIn(): boolean {
    const token = this.tokenService.getToken();
    if (token) {
      return true;
    }
    return false;
  }

  logout() {
    this.tokenService.deleteToken();
    this.router.navigate(['home']);
  }
}
