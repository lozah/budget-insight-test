import {ISerializer} from './ISerializer';
import {HttpClient} from '@angular/common/http';
import {Account} from '../models/Account';

export class AccountsSerializer implements ISerializer {

    constructor() {}
    fromJson(json: any): any {
        const accounts: Account[] = [];
        json.forEach(account => {
            accounts.push(
                new Account(
                    account.formatted_balance,
                    account.number,
                    account.original_name
                )
            );
        });
        return accounts;
    }

    toJson(object: any): any {
    }

}
