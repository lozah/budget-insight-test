export interface ISerializer {
    fromJson(json: any): any;
    toJson(object: any): any;
}
