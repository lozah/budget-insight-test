import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-webview-launcher',
  templateUrl: './webview-launcher.component.html',
  styleUrls: ['./webview-launcher.component.css']
})
export class WebviewLauncherComponent implements OnInit {
  @Output() emitLaunchWebview = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onLaunchWebview() {
    this.emitLaunchWebview.emit();
  }
}
