import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../core/api/http-services/account.service';
import {tap} from 'rxjs/operators';
import {AuthService} from '../../core/auth/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  accounts: Account[];
  constructor(
      private accountService: AccountService,
      private authService: AuthService
  ) { }

  ngOnInit() {
    this.accountService.getUserAccounts()
        .pipe(
            tap(accounts => this.accounts = accounts)
        )
        .subscribe();
  }
  
  logout() {
    this.authService.logout();
  }
}
