import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import { TokenService } from '../../core/auth/services/token.service';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  token: string;
  error: string;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.getQueryParams();
  }

  getQueryParams() {
    this.route.queryParams
        .pipe(
            tap(params => {
              this.error = params.error;
              this.token = params.code;
              if (this.error || !this.token) {
                return;
              }
              this.handleTokenParam();
            })
        )
        .subscribe();
  }

  handleTokenParam() {
    this.tokenService.setToken(this.token);
    this.router.navigate(['dashboard']);
  }

  launchWebview() {
    const webViewEnv = environment.webview;
    const webViewUri = `${webViewEnv.url}?response_type=code&client_id=${webViewEnv.clientId}&redirect_uri=${webViewEnv.redirectUri}`;
    window.open(webViewUri, '_self');
  }
}
